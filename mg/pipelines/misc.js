const arrayToRelation = (collIn, toCollName, fieldName, match=[], comment= 'arrayToRelation') => {
  const dollarFieldName = `$${fieldName}`;
  const pl = [
    ...match,
    { $project: { [fieldName]: 1 } },
    { $unwind: dollarFieldName },
    { $project: { _id: { p: '$_id', c: dollarFieldName } } },
    { $out: toCollName },
  ];
  return pl;
};

const bucketSample = (buckets = 10, sampleSize = 1000, onField = '_id') => {
  return [
    ...(sampleSize === 0) ? [] : [{ $sample: { size: sampleSize } }],
    { $bucketAuto: { groupBy: `$${onField}`, buckets } },
  ];
};

export {
  arrayToRelation,
  bucketSample,
};

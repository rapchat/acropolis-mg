/* eslint-disable quote-props */
/* eslint-disable no-unused-expressions */
[
  {
    '$set': {
      'dtObj': {
        'y': {
          '$year': '$dt',
        },
        'm': {
          '$month': '$dt',
        },
        'd': {
          '$dayOfMonth': '$dt',
        },
        'h': {
          '$hour': '$dt',
        },
      },
    },
  },
]

cl.updateOne({ "_id" : ObjectId("5d661ace1eaab7429d445ee9")}, [ {$set : { dtObj:  {y: {$year : '$dt'}, m: {$month: '$dt'}, d: {$dayOfMonth: '$dt'}, h:{$hour: '$dt'}    }}}])


const facet = (facetArr) => {
  /**
   * facetArr:  [ [facetName, pipeline] [facetName, pipeline] ...etc ]
   */
  return { $facet: Object.assign(...facetArr.map(d => ({ [d[0]]: d[1] }))) };
};

const group = (fieldNameOrArray, rest = {}) => {
  const normalizeFN = fn => fn.replace('.', '_');
  const _id = () => {
    if (typeof fieldNameOrArray === 'string' ) {
      return `$${fieldNameOrArray}`;
    }
    const rt = {};
    // eslint-disable-next-line no-return-assign
    fieldNameOrArray.forEach(fn => rt[normalizeFN(fn)] = `$${fieldNameOrArray}`);
    return rt;
  };
  return [{ $group: Object.assign({ _id: _id() }, rest) }];
};

const distinct = fieldNameOrArray => group(fieldNameOrArray);

const distinctCount = fieldNameOrArray => group(fieldNameOrArray, { count: { $sum: 1 } });

export {
  facet,
  group,
  distinct,
  distinctCount,
};

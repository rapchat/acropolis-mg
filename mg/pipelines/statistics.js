
/**
 * useful when distinct operator exceeds limits
 * @param {*} fieldName
 * returns [ { distinct: 0.642 }, { distinct: 0.7936 },{ distinct: 0.0707 },
 * we don't use $addToSet for efficiency
 */
const distinct = (fieldName = '_id') => {
  /**
   * useful when distinct operator exceeds limits
   *
  */
  return [
    { $group: { _id: `$${fieldName}`, distinct: { $first: `$${fieldName}` } } },
    { $project: { distinct: 1, _id: 0 } },
  ];
};

const PearsonR = async (fx, fy) => {
  /** fx 1st fieldName
   *  fy 2nd fieldName
   *  Returns: Pearsons (linear) Correlation Coefficent  {'fx': "prc", "fy": "pa","R": 0.8018350824109506}
  */
  const x = `$${fx}`;
  const y = `$${fy}`;
  const sumcolumns = {
    $group: {
      _id: true,
      count: { $sum: 1 },
      sumx: { $sum: x },
      sumy: { $sum: y },
      sumxSquared: { $sum: { $multiply: [x, x] } },
      sumySquared: { $sum: { $multiply: [y, y] } },
      sumxy: { $sum: { $multiply: [x, y] } },
    },
  };
  const multiplySumxSumy = { $multiply: ['$sumx', '$sumy'] };
  const multiplySumxyCount = { $multiply: ['$sumxy', '$count'] };
  const partOne = { $subtract: [multiplySumxyCount, multiplySumxSumy] };
  const multiplysumxSquaredCount = { $multiply: ['$sumxSquared', '$count'] };
  const sumxSquared = { $multiply: ['$sumx', '$sumx'] };
  const subPartTwo = { $subtract: [multiplysumxSquaredCount, sumxSquared] };

  const multiplySumySquaredCount = { $multiply: ['$sumySquared', '$count'] };
  const sumySquared = { $multiply: ['$sumy', '$sumy'] };
  const subPartThree = { $subtract: [multiplySumySquaredCount, sumySquared] };

  const partTwo = { $sqrt: { $multiply: [subPartTwo, subPartThree] } };
  const pearson = { $project: { _id: 0, fx, fy, R: { $divide: [partOne, partTwo] } } };
  return [sumcolumns, pearson];
};

export {
  PearsonR,
  distinct,
};


const arrayToRelation = (collIn, toCollName, fieldName, match=[], comment= 'arrayToRelation') => {
  const dollarFieldName = `$${fieldName}`;
  const pl = [
    ...match,
    { $project: { [fieldName]: 1 } },
    { $unwind: dollarFieldName },
    { $project: { _id: { p: '$_id', c: dollarFieldName } } },
    { $out: toCollName },
  ];
  return pl;
};

export {
  arrayToRelation,
};

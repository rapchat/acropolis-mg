/* eslint-disable no-await-in-loop */
/* eslint-disable no-param-reassign */
const doc = {
  pdd: 4276,
  processing: 49,
  ring_time: 8013,
  release_side: 'O',
  response_ingress: '200 OK',
  response_ingress_code: 200,
  response_egress: '200 OK',
  response_egress_code: 200,
  release_cause: 'Answered',
  duration: 8,
  egress_duration: 8,
  ingress_billtime: 12,
  egress_billtime: 12,
  ingress_cost: 0.0016,
  egress_cost: 0.0003,
  credit: 999999.9984,
  credit_start: 1000000,
  profit: 0.0013,
  profit_percent: 433.3333,
  egress_trunk_trace: [{ egress_rate_id: 36390627,
    egress_trunk_id: 35,
    egress_trunk_name: 'example trunk_name',
    egress_min_duration: 0,
    egress_max_duration: 7200,
    egress_rate_table_id: 235,
    egress_rate_table_name: 'example rate_table_name',
    egress_lrn: 1,
    egress_carrier_id: 39,
    egress_carrier_name: 'a carrier',
    egress_bill_increment: 6,
    egress_bill_start: 6,
    term_code_country: 'USA',
    term_code_name: null,
    term_code: '1510549',
    egress_rate: 0.0013,
    egress_rate_type: 'I',
    egress_tech_prefix: '08245#',
    term_ip: '98.158.150.100',
    term_dst_number: '08245#15105499059',
    try_timeout: 2,
    pdd_timeout: 6,
    egress_decimal_points: 4,
    term_ani: '5106352985',
    ts: 1556922852518.0,
    egress_pdd: 4223,
    ring_time: 8013,
    term_codecs: ['G729/8000', 'PCMU/8000', 'telephone-event/8000'],
    response_egress_code: 200,
    response_egress: '200 OK',
    release_cause: 'Answered',
    end_ts: 1556922864754.0,
    last: 1 }],
  answer_time: 1556922864754.0,
  end_time: 1556922872064.0,
};

const insertsIOT = async (coll, { from = 1, to = 10000, w = 1, logger } = {}) => {
  const startTime = new Date();
  const stats = (i) => {
    const elapsedSeconds = (new Date() - startTime) / 1000;
    const cnt = i - from;
    const statsObj = { insertsIOT: 1, cnt, opsPerSec: parseInt(cnt / elapsedSeconds, 10) };
    if (logger) { logger.info(statsObj); }
    return statsObj;
  };
  const getCallId = n => `CallId_${n}`;
  try {
    for (let i = from; i <= to; i += 1) {
      doc.call_id = getCallId(i);
      await coll.insertOne(doc, { w });
      if (i === to) { stats(i); }
    }
  } catch (err) {
    if (logger) { logger.error({ benchmarkInsert: 0, err }); }
  }
};

export {
  insertsIOT,
};

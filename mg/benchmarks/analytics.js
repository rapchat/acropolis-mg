/* eslint-disable no-await-in-loop */
/* eslint-disable no-param-reassign */

import { DateRandom, dtToStrCompressed, dtTObjUtc } from 'acropolis-nd/hellas/athena.js';

const insertDates = async (coll, { name = '01', maxCnt = 10000, reportEvery = 1000, w = 0, logger } = {}) => {
  const dtRnd = new DateRandom(new Date(2017, 0), new Date(2018, 9));
  const startTime = new Date();
  let cnt = 0;
  const stats = () => {
    const elapsedSeconds = (new Date() - startTime) / 1000;
    const statsObj = { insertsDates: name, cnt, opsPerSec: parseInt(cnt / elapsedSeconds, 10) };
    if (logger) { logger.info(statsObj); }
    return statsObj;
  };
  while (cnt < maxCnt) {
    const rd = dtRnd.randomDt;
    await coll.insertOne({ dt: rd, dtStr: dtToStrCompressed(rd), dtObj: dtTObjUtc(rd) }, { w });
    if (cnt % reportEvery === 0) { stats(); }
    cnt += 1;
  }
  return stats();
};

export {
  insertDates,
};

/* eslint-disable no-underscore-dangle */
/*
* critical events
* [closed, serverClosed, serverDescriptionChanged]
* to inject it into req see: https://hackprogramming.com/how-to-configure-a-mongodb-connection-in-expressjs-for-production/
# @todo: implement pkFactory
*/

/** 
 * Importing CommonJS from ESM is not supported in Node for legacy packages and require we have 2 options;
 *  1 import complete package and get it from default exports:
 *      import * as mongodb from 'mongodb';
 *      const { MongoClient } = mongodb.default
 *  2 or use createRequire:
 *      import {createRequire} from 'module';
 *      const require = createRequire(import.meta.url);
 *      const { MongoClient } = require('mongodb');
 */

import { eventsHandle } from 'acropolis-nd/hellas/delphi.js';
import { MongoClient } from '../../legacy-export.js';

const mgClientEventsCritical = ['serverClosed', 'close', 'error', 'timeout'];
const mgClientEventsFrequent = ['serverHeartbeatSucceeded', 'serverHeartbeatStarted', 'serverOpening']; // Frequent events

class MgClientPlus {
  constructor({ name = 'connLocal', connUri = null, connOptions = {}, logger = null } = {}) {
    const options = Object.assign({ useUnifiedTopology: true, forceServerObjectId: true, poolSize: 200, useNewUrlParser: true }, connOptions); // some sensible defaults
    const connUriToUse = connUri || process.env[name] || 'mongodb://localhost:27017';
    this._props = { name, connUri: connUriToUse, connOptions: options, logger, client: undefined, connected: false };
  }

  get name() { return this._props.name; }

  get connUri() { return this._props.connUri; }

  get connOptions() { return this._props.connOptions; }

  get client() { return this._props.client; }

  get logger() { return this._props.logger; }

  get eventsTypical() { return this.events.filter(x => (!mgClientEventsFrequent.includes(x) && !mgClientEventsCritical.includes(x))); }

  get events() { return Object.keys(this.client.topology._events); }

  async connect() {
    try {
      if (this._props.connected === false) {
        this._props.connected = true;
        this._props.client = await MongoClient.connect(this.connUri, this.connOptions);
        if (this.logger && this.client) {
          this.logger.info(`mongo established connection:${this.name} uri:${this.connUri}`);
          eventsHandle(this.client, this.eventsTypical, { name: this.name, logFun: (x => this.logger.info(x)), verbose: 0 });
          eventsHandle(this.client, mgClientEventsCritical, { name: this.name, logFun: (x => this.logger.error(x)), verbose: 0 });
        }
      }
      return this;
    } catch (err) {
      this._props.client = false;
      if (this.logger) {
        this.logger.error(err.mesage);
      }
      throw (err);
    }
  }

  getDb(dbName) { return this.client.db(dbName); }

  getCollection(dbName, collName) { return this.client.db(dbName).collection(collName); }

  async destroy(reason = 'unknown') {
    try {
      if (this.client !== undefined) {
        await this.client.close();
        this._props.client = undefined;
        this._props.connected = false;
        return { client: this.name, closed: reason };
      }
      return { client: this.name, was_closed: 1 };
    } catch (err) {
      throw err;
    }
  }
}

export {
  MgClientPlus,
  mgClientEventsCritical,
  mgClientEventsFrequent,
};

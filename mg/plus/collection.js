
import { bucketSample } from '../pipelines/misc.js';

class MgCollPlus {
  constructor(collection, { logger } = {}) {
    this._collection = collection;
    this._logger = logger;
  }

  get collection() { return this._collection; }

  get name() { return this.collection.name; }

  get namespace() { return this.collection.namespace; }

  get logger() { return this._logger; }

  // -------------------------------------------------------------------------- bucket operations
  async bucketQueries(buckets = 10, sampleSize = 1000, onField = '_id') {
    const pipeline = bucketSample(buckets, sampleSize, onField);
    const groups = await this.collection.aggregate(pipeline).toArray();
    let elCnt = 0;
    const queries = groups.map((el) => {
      elCnt += 1;
      if (elCnt === groups.length) { return { _id: { $gte: el._id.min } }; } // first so works with buckets=1
      if (elCnt === 1) { return { _id: { $lt: el._id.max } }; }
      return { _id: { $gte: el._id.min, $lt: el._id.max } };
    });
    return queries;
  }

  async bucketPipeline({ buckets = 10, sampleSize = 1000, pipeline = [] } = {}) {
    const bq = await this.bucketQueries(buckets, sampleSize);
    return bq.map(el => ([{ $match: el }, ...pipeline]));
  }

  /**
   *
   * @param {*} param0;
   * returns [AggregationCursor]
   */
  async scan({ buckets = 10, sampleSize = 10000, pipeline = [], options = { comment: this.name } } = {}) {
    /**
     * Create a command to provide query bounds for partitioning data in a collection https://jira.mongodb.org/browse/SERVER-24274
     * https://jira.mongodb.org/browse/SERVER-33998
     */
    let plArr = await this.bucketPipeline({ buckets, sampleSize, pipeline });
    plArr = plArr.map((el) => {
      return this.collection.aggregate(el, options);
    });
    return plArr;
  }
  // --------------------------------------------------------------------------
}

export {
  MgCollPlus,
};

/* eslint-disable no-await-in-loop */
/* eslint-disable no-unused-vars */

// const acropolis = require('acropolis-nd');
// const { delos, delphi } = require('acropolis-nd');
import { delos, delphi } from 'acropolis-nd';

import {
  pipelinesStatistics,
  pipelinesMisc,
  plusClient,
  plusCollection,
  modelsConfig,
  modelsUtils,
  benchmarksIOT,
  pagination,
} from './index.js';

/*
const {
  pipelinesStatistics,
  pipelinesMisc,
  plusClient,
  plusCollection,
  modelsConfig,
  modelsUtils,
  benchmarksIOT,
  pagination,
} = require('./index');
*/
const { MgClientPlus } = plusClient;
const { insertsIOT } = benchmarksIOT;
const { Pagination } = pagination;


const mongoUri = 'mongodb://localhost:27517';

const logger = console;

const initTestData = async (dropDb = true) => {
  const paginationSimple = async (coll) => {
    for (let i = 1; i <= 10; i += 1) {
      await coll.updateOne({ _id: i }, { $inc: { cnt: 1 } }, { upsert: true });
    }
  };
  const mgClient = new MgClientPlus({ name: 'testMgCl', connUri: mongoUri, logger: console });
  try {
    await mgClient.connect();
    const testDb = mgClient.getDb('db_test');
    const paginationSimpleColl = testDb.collection('paginationSimple');
    if (dropDb === true) {
      const result = await testDb.dropDatabase();
      logger.info({ initTestData: 'dropDb', result });
      await insertsIOT(testDb.collection('iot'), { logger: console });
      await paginationSimple(paginationSimpleColl);
    }
    return mgClient;
  } catch (err) {
    logger.error({ initTestData: 'err', err });
    return err;
  }
};

const testConn = async () => {
  const mgClient = new MgClientPlus({ name: 'testMgCl', connUri: mongoUri, logger: console });
  try {
    await mgClient.connect();
    // if (this.logger && this.client) { eventsLog(this.client, mgClientEvents, this.name, this.logger, 1); }
    // console.log({ events: this.events })
    //  console.log({ client: mgClient.client })
    const testColl = mgClient.getCollection('db_test', 'col_test');
    const insertResult = await testColl.findOneAndUpdate({ foo: 'zzz' }, { $set: { foo2: 'bar2' } }, { upsert: true, returnOriginal: true });
    // console.log( { insertResult: insertResult });
    // console.log( { testColl });
    return mgClient;
  } catch (err) {
    logger.error({ testConn: 'err', err });
    return err;
  }
};

const testPaging = async () => {
  const objJson = obj => JSON.stringify(obj);
  const contentArr = arr => arr.map(x => x._id);
  const mgClient = await testConn();
  const pgColl = mgClient.getCollection('db_test', 'paginationSimple');
  const contents = await pgColl.find({}, { projection: { _id: 1 } }).toArray();
  logger.info({ contents: contentArr(contents) });
  const test = async (pageSize = 2) => {
    const pagObj = new Pagination({ pageSize, docMax: { _id: 99999 } });
    let query = {};
    let queryRes;
    let pagRes;
    let direction = 1;
    let i = 0;
    do {
      i += 1;
      logger.log(delos.strSizeAlignRight('*', 20, '*'), i);
      queryRes = await pgColl.find(query, { limit: pageSize, sort: { _id: direction } }).toArray();
      pagRes = pagObj.scroll(queryRes, direction);
      logger.info({ i, query: objJson(query), contents: contentArr(queryRes), pagRes: objJson(pagRes) });
      query = pagRes.pageNext;
      // console.log([i, query]);
    } while (query !== null);
    logger.log(delos.strSizeAlignRight('scrillUp', 40, '*'));
    i = 0;
    query = pagRes.pagePrevious;
    // query = { _id: { $lt: 11 } };
    direction = -1;
    do {
      i += 1;
      logger.log(delos.strSizeAlignRight('*', 20, '*'), i);
      queryRes = await pgColl.find(query, { limit: pageSize, sort: { _id: direction } }).toArray();
      pagRes = pagObj.scroll(queryRes, direction);
      logger.info({ i, query: objJson(query), contents: contentArr(queryRes), pagRes: objJson(pagRes) });
      query = pagRes.pagePrevious;
      // console.log([i, query]);
    } while (query !== null);
    queryRes = await pgColl.find({ _id: { $gt: 0 } }, { limit: pageSize, sort: { _id: 1 } }).toArray();
    // headersArr(dataArr, direction = 0, { pathURL = '', expressResponse, otherQs, encode = true }
    const headersArr = pagObj.headersArr(queryRes, i, { encode: false });
    logger.log( { headersArr });
  }
  await test(3);
  // res = await 

  // console.info({ mgClient });
};
// ============================================================================
logger.log(delos.strSizeAlignRight('Test Start ', 24, '*'));
const client = initTestData(false);
logger.log(delos.strSizeAlignRight('testPaging  ', 24, '*'));
testPaging();
// testConn();

console.log({ client });

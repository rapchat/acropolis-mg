module.exports = {
  env: {
    es6: true,
    node: true,
  },
  extends: [
    'airbnb-base',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  rules: {
    "max-len": [1, 160, 2, {ignoreComments: true}],
    "no-underscore-dangle": ["error", {"allowAfterThis": true, "allow": ["_id"]  }],
    "arrow-body-style": 0,
    "prefer-spread": ["error"],
    "object-curly-newline": ["error", {"consistent": true}],
    "import/extensions": ["error", "never", "ignorePackages"],
    "import/prefer-default-export": 'off',
  },
};

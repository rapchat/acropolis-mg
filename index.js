/* eslint-disable global-require */

/*

module.exports = {
  pipelinesStatistics: require('./mg/pipelines/statistics'),
  pipelinesMisc: require('./mg/pipelines/misc'),
  plusClient: require('./mg/plus/client'),
  plusCollection: require('./mg/plus/collection'),
  modelsConfig: require('./mg/models/config'),
  modelsUtils: require('./mg/models/utils'),
  benchmarksIOT: require('./mg/benchmarks/iot'),
  pagination: require('./mg/models/pagination'),
  atlas: require('./mg/atlas.js'),
};

*/

import * as pipelinesStatistics from './mg/pipelines/statistics.js';
import * as pipelinesMisc from './mg/pipelines/misc.js';
import * as plusClient from './mg/plus/client.js';
import * as plusCollection from './mg/plus/collection.js';
import * as modelsConfig from './mg/models/config.js';
import * as modelsUtils from './mg/models/utils.js';
import * as benchmarksIOT from './mg/benchmarks/iot.js';
import * as pagination from './mg/models/pagination.js';
import * as atlas from './mg/atlas.js';


export {
  pipelinesStatistics,
  pipelinesMisc,
  plusClient,
  plusCollection,
  modelsConfig,
  modelsUtils,
  benchmarksIOT,
  pagination,
  atlas,
};
